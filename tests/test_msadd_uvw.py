import numpy
import pytest
import tarfile
import datetime

from math import acos

from casacore import tables
from casacore.measures import measures
from casacore.quanta import quantity

from ska.sdp.msadd.utils import get_uvw, get_antenna_uvw, get_uvw_J2000
from ska.sdp.msadd.uvw import UpdateUvwColumn

from astropy import units
from astropy.coordinates import Angle
from astropy.time import Time

from ska.sdp.msadd import msadd_uvw

def do_compare(test_1, test_2):

    diff = test_1 - test_2
    if numpy.linalg.norm(diff) == 0:
        return True

    print(test_1)
    print(test_2)

    len_1 = (numpy.linalg.norm(test_1))
    len_2 = (numpy.linalg.norm(test_2))

    unit_1 = test_1 / len_1
    unit_2 = test_2 / len_2

    cos_separation = unit_1.dot(unit_2)

    angle_sep = acos(cos_separation)
    print("Angular separation of baselines: ", angle_sep)
    assert angle_sep < 0.00029, "Angular separation greater than 1 minute of arc"

    len_1 = (numpy.linalg.norm(test_1))
    len_2 = (numpy.linalg.norm(test_2))
    diff = abs(len_1 - len_2)
    print("Baselines differ in length by: ", diff, " m ")
    stretch = (len_1 / len_2) - 1.0
    print("Stretch: ", stretch)

    assert diff < 1.0e-3
@pytest.fixture
def from_askap_ms():
    ms = "tests/testdata/1934_SB4094_b0_t0_ch0.ms"

    tar = tarfile.open(ms + ".tar.gz", "r:gz")
    tar.extractall("tests/testdata/")

    ants = tables.table(ms + "/ANTENNA", readonly=True)
    pos_0 = ants.getcell('POSITION', 0)
    pos_1 = ants.getcell('POSITION', 1)

    field = tables.table(ms + "/FIELD", readonly=True)
    target = field.getcol('DELAY_DIR', 0, 1, 1)  # direction in rad
    main = tables.table(ms, readonly=True)
    time_centroid = main.getcol('TIME', 1, 1, 1)  # Time in MJD Secs

    target_ra = Angle(target[0][0][0] * units.rad)
    target_dec = Angle(target[0][0][1] * units.rad)

    print(target_ra.hms, target_dec.dms)

    # Lets get the Time
    print(target_ra)
    time = Time(time_centroid / (3600. * 24), format='mjd')
    print(type(time))
    print(time)
    print(time.value)

    # lets get the MS from the measurement set - this one has autos
    uvw_from_ms = main.getcol('UVW', 1, 1, 1)

    return [pos_0, pos_1, time, uvw_from_ms, target_ra, target_dec]


def get_uvw_from_ms():
    ms = "tests/testdata/1934_SB4094_b0_t0_ch0.ms"
    with tables.table(ms, readonly=True) as main:
        uvw_from_ms = main.getcol('UVW', 1, 1, 1)
    return uvw_from_ms




def test_msadd_uvw_with_input_file_no_direction(from_askap_ms):
    pos_0 = from_askap_ms[0]
    pos_1 = from_askap_ms[1]
    time_val = from_askap_ms[2]
    uvw_from_ms = from_askap_ms[3]
    target_ra = from_askap_ms[4]
    target_dec = from_askap_ms[5]

    # run the application
    msadd_uvw.main(["--input", "tests/testdata/1934_SB4094_b0_t0_ch0.ms/ANTENNA", "--output",
                    "tests/testdata/1934_SB4094_b0_t0_ch0.ms"])
    # check the uvw were updated

    n_uvw_from_ms = get_uvw_from_ms()

    # calculate what they should be:
    uvw_1 = get_antenna_uvw(pos_0, time_val, target_ra, target_dec, swap_baselines=False)
    uvw_2 = get_antenna_uvw(pos_1, time_val, target_ra, target_dec, swap_baselines=False)

    test_1 = uvw_2 - uvw_1
    test_2 = numpy.array([n_uvw_from_ms[0][0], n_uvw_from_ms[0][1], n_uvw_from_ms[0][2]])

    do_compare(test_1,test_2)

def test_msadd_uvw_with_locations_local(from_askap_ms):

    # run the application
    msadd_uvw.main(["--locations", "tests/testdata/TSI-AP.json", "--output",
                    "tests/testdata/1934_SB4094_b0_t0_ch0.ms"])
    # check the uvw were updated

    n_uvw_from_ms = get_uvw_from_ms()

    test_1 = numpy.array([n_uvw_from_ms[0][0], n_uvw_from_ms[0][1], n_uvw_from_ms[0][2]])

    expecting = [130.83592688, 54.27422394, 277.58757832]

    do_compare(test_1,expecting)


def test_msadd_uvw_with_locations_url(from_askap_ms):

    # run the application
    msadd_uvw.main(["--locations", "https://gitlab.com/ska-telescope/sdp/ska-sdp-tmlite-repository/-/raw/main/data/instrument/ska1_low/layout/LOW_AA0.5_test.json", "--output",
                    "tests/testdata/1934_SB4094_b0_t0_ch0.ms"])
    # check the uvw were updated

    n_uvw_from_ms = get_uvw_from_ms()

    test_1 = numpy.array([n_uvw_from_ms[0][0], n_uvw_from_ms[0][1], n_uvw_from_ms[0][2]])

    expecting = [130.83592688, 54.27422394, 277.58757832]

    do_compare(test_1, expecting)

def test_msadd_uvw_with_locations_url_and_directions(from_askap_ms):
    # run the application
    msadd_uvw.main(["--locations",
                    "https://gitlab.com/ska-telescope/sdp/ska-sdp-tmlite-repository/-/raw/main/data/instrument/ska1_low/layout/LOW_AA0.5_test.json",
                    "--output",
                    "tests/testdata/1934_SB4094_b0_t0_ch0.ms",
                    "--delay",
                    "19:39:25.026 -63:42:45.000",
                    "--frame",
                    "icrs"])


    n_uvw_from_ms = get_uvw_from_ms()

    test_1 = numpy.array([n_uvw_from_ms[0][0], n_uvw_from_ms[0][1], n_uvw_from_ms[0][2]])

    expecting = [130.83592688, 54.27422394, 277.58757832]

    do_compare(test_1, expecting)