#!/usr/bin/env python3
import json
import tarfile

import pytest
from ska.sdp.msadd.antenna import AntennaTable
from ska.sdp.msadd.utils import get_uvw

from casacore import tables


# These tests are just testing interfaces to the test data sets
# It is expected we will have to be able to deal with multiple formats
# First just test the basic framework is working.

@pytest.fixture
def get_table_updater_with_ms():
    in_table = "tests/testdata/low_sim.ms/ANTENNA"
    new_table_updater = AntennaTable(input_table_name=in_table)
    return [new_table_updater]

@pytest.fixture
def get_table_updater_with_local_json():
    jsonfile = "tests/testdata/TSI-AP.json"
    new_table_updater = AntennaTable(json_file_location=jsonfile)
    return [new_table_updater]
@pytest.fixture
def get_table_updater_with_local_json_geocentric():
    jsonfile = "tests/testdata/layout.json"
    new_table_updater = AntennaTable(json_file_location=jsonfile)
    return [new_table_updater]
@pytest.fixture
def get_table_updater_with_url():
    url = "https://gitlab.com/ska-telescope/sdp/ska-sdp-tmlite-repository/-/raw/main/data/instrument/ska1_low/layout/LOW_AA0.5_test.json"
    new_table_updater = AntennaTable(json_file_location=url)
    return [new_table_updater]

def test_get_antenna_table_from_json():
    # open Antenna Locations
    jsonfile = "tests/testdata/TSI-AP.json"

    with open(jsonfile) as f:
        antenna_table = json.load(f)
    print(antenna_table)


def test_get_antenna_table_from_ms():
    ms = "tests/testdata/low_sim.ms"
    tar = tarfile.open(ms + ".tar.gz", "r:gz")
    tar.extractall("tests/testdata/")
    tar.close()
    # open MAIN Table in Input MS
    t = tables.table(ms + "/ANTENNA", readonly=True)

    pos = t.getcell('POSITION', 0)

    assert (pos[0] == -2564976.056533818)


def test_antenna_table_get_antenna_pos(get_table_updater_with_ms):
    t = get_table_updater_with_ms[0]
    pos = t.get_antenna_pos(0)
    assert (pos[0] == -2564976.056533818)

def test_antenna_table_json(get_table_updater_with_local_json):
    t = get_table_updater_with_local_json[0]
    print(t.antenna_table)
    pos = t.get_antenna_pos(0)
    assert (pos[0] == -2563226.960308)

def test_antenna_table_json_geo(get_table_updater_with_local_json_geocentric):
    t = get_table_updater_with_local_json_geocentric[0]
    print(t.antenna_table)
    pos = t.get_antenna_pos(0)
    assert (round(pos[0].value,3) == -2564976.057)


def test_antenna_table_url(get_table_updater_with_url):
    t = get_table_updater_with_url[0]
    print(t.antenna_table)
    pos = t.get_antenna_pos(0)
    assert (pos[0] == -2563226.960308)