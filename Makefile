# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c

include .make/base.mk
include .make/oci.mk

all: test lint

# The following steps copy across useful output to this volume which can
# then be extracted to form the CI summary for the test procedure.
test:
	pip3 install -r test-requirements.txt
	mkdir -p build || true
	pytest | tee ./build/setup_py_test.stdout
	mv coverage.xml ./build/reports/code-coverage.xml

# The following steps copy across useful output to this volume which can
# then be extracted to form the CI summary for the test procedure.
lint:
	pip3 install -r lint-requirements.txt
	mkdir -p build/reports || true
	pylint --output-format=parseable ska | tee ./build/code_analysis.stdout
	pylint --output-format=pylint2junit.JunitReporter ska > ./build/reports/linting.xml

.PHONY: all test lint
