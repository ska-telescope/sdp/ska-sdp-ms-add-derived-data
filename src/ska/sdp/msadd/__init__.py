# -*- coding: utf-8 -*-

"""Module init code."""


__version__ = '0.1.2'

__author__ = 'Stephen Ord'
__email__ = 'stephen.ord@csiro.au'
__all__ = (
    # sub packages
    "utils",
    "uvw",
    "antenna",
    # applications
    "msadd_uvw"
)
from .antenna import AntennaTable
from .uvw import UpdateUvwColumn
