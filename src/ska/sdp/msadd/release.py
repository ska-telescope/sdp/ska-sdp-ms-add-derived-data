# -*- coding: utf-8 -*-
#
# This file is part of the MSADD Softwarer Pacakge 
#
#
#
"""Release information for ska_sdp_msadd Python Package."""

name = """ska_sdp_masadd"""
version = "0.1.2"
version_info = version.split(".")
description = """A Package to Add Derived Data to Measurement Sets."""
author = "Team YANDA"
author_email = "stephen.ord at csiro.au"
url='https://github.com/ska-telescope/ska-sdp-ms-add-derived-data',
copyright = """UWA and CSIRO"""
