# =============================================================================
# Visibility Send/Receive image : build stage
# =============================================================================
FROM debian:bullseye-slim AS buildenv
LABEL \
      author="Stephen Ord" \
      description="An toolset for adding data to measurement sets" \
      license="BSD-3-Clause" \
      vendor="SKA Telescope" \
      org.skatelescope.team="YANDA" \
      org.skatelescope.website="https://gitlab.com/ska-telescope/ska-sdp-ms-add-derived-data/"
ARG PYPI_REPOSITORY_URL=https://artefact.skao.int/repository/pypi-internal

RUN apt-get update && apt-get install -y python3-pip && python3 -m pip install -U pip
# Checkout our project and install it
RUN apt-get install -y wget casacore-data
WORKDIR /var/lib/casacore/data
RUN wget ftp://ftp.astron.nl/outgoing/Measures/WSRT_Measures.ztar
RUN mv WSRT_Measures.ztar WSRT_Measures.tar.gz
RUN gunzip WSRT_Measures.tar.gz
RUN tar -xvf WSRT_Measures.tar
RUN rm WSRT_Measures.tar
COPY . /app
WORKDIR /app
RUN pip3 install -r docker-requirements.txt
RUN python3 -m pip install --extra-index-url=$PYPI_REPOSITORY_URL/simple -e .

